#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/FOTAKernel:17233920:f1dfb3982eb2a049bfd2dcbfd42d3acb383a2674; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:12132352:c5072ae0e32378dfc06ec7137de4c7a931f7c049 EMMC:/dev/block/bootdevice/by-name/FOTAKernel f1dfb3982eb2a049bfd2dcbfd42d3acb383a2674 17233920 c5072ae0e32378dfc06ec7137de4c7a931f7c049:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
