#!/bin/bash

cat system/app/LatinIME/LatinIME.apk.* 2>/dev/null >> system/app/LatinIME/LatinIME.apk
rm -f system/app/LatinIME/LatinIME.apk.* 2>/dev/null
cat system/product/app/webview/webview.apk.* 2>/dev/null >> system/product/app/webview/webview.apk
rm -f system/product/app/webview/webview.apk.* 2>/dev/null
