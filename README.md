## F5122-user 8.0.0 34.4.A.0.364 2371171945 release-keys
- Manufacturer: sony
- Platform: 
- Codename: suzu
- Brand: Sony
- Flavor: lineage_suzu-userdebug
- Release Version: 9
- Id: PQ3A.190801.002
- Incremental: eng.chippa.20200722.140438
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: 480
- Fingerprint: Sony/F5122/F5122:8.0.0/34.4.A.0.364/2371171945:user/release-keys
- OTA version: 
- Branch: F5122-user-8.0.0-34.4.A.0.364-2371171945-release-keys
- Repo: sony_suzu_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
